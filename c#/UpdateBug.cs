try
{
    if(args.IsNull())
    {
        return "参数不能为空";
    }
    if(args.Id <= 0)
    {
        return "Id不能为空";
    }
    var info = _people.Get(args.Id);
    if(info != null)
    {
        info.Id = args.Id; // 更新人员Id也更新了
    }
}