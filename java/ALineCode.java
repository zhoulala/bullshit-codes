package spring;


public class B {


    public static void main(String[] args)  {
        System.out.println(isTwoPower(15));
    }

    /**
     * 看不懂的代码
     * @param n
     * @return
     */
    public static boolean isTwo(int n){
        // 其实是用于判断入参是否为2的幂次方
        return (n & n-1) == 0;
    }

}
